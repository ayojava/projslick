Sample Slick slickDb.Project 
=================================

The project demonstrates the basic functionalities to Create , Read , Update , Delete using Slick with  mysql Database.

![](https://img.shields.io/badge/mysql-8.0-blue.svg) 
![](https://img.shields.io/badge/slick-3.0-green.svg) 
![](https://img.shields.io/badge/scala-2.3-yellow.svg) 

#Steps

**1** Clone the project 

**2** Open the application.conf , edit the database configuration for mysqlDB

**3** Each of the classes are meant to be run as a standalone , however the CreateSchema class should be run first as it creates the tables that subsequent examples would be run on

**NB** This project is a work-in-progress so from time to time I would be making changes to the code and adding more examples.

#Using Flyway Migration Script

**1** Alternatively, the tables can be created using the migration script in db/migration/V1__Create_Schema.sql .

**2** Edit the flyway specific configurations (flywayUrl,flywayUser,flywayPassword,flywayLocations) in the build.sbt file 

**3** From the project folder , type "sbt flywayMigrate" then press Enter  OR if from IntelliJ SBT Shell type " flywayMigrate "

**4** Thanks to this earlier writeup by https://blog.knoldus.com/flyway-with-sbt/ and https://www.lewuathe.com/db-migration-with-flyway.html
