resolvers += "Flyway" at "https://flywaydb.org/repo"

addSbtPlugin("io.github.davidmweber" % "flyway-sbt" % "5.2.0")