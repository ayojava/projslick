create table `EmployeeProject` (`employeeId` BIGINT NOT NULL,`projectId` BIGINT NOT NULL);

alter table `EmployeeProject` add constraint `employee_project_pk` primary key(`employeeId`,`projectId`);

create table  `EmployeeTable` (`firstName` TEXT NOT NULL,`lastName` TEXT NOT NULL,`gender` TEXT NOT NULL,`employeeLevel` TEXT NOT NULL,`resumptionDate` TEXT NOT NULL,`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY);

create table  `ProjectTable` (`name` TEXT NOT NULL,`startDate` TEXT,`duration` BIGINT,`projectStatus` TEXT NOT NULL,`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY);

create table `Suppliers` (`name` TEXT NOT NULL,`street` TEXT NOT NULL,`city` TEXT NOT NULL,`state` TEXT NOT NULL,`zip` TEXT,`createDate` TEXT NOT NULL,`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY);

create table `Coffees` (`name` VARCHAR(512) NOT NULL,`price` DOUBLE NOT NULL,`qty` BIGINT NOT NULL,`supplierId` BIGINT NOT NULL,`createDate` TEXT NOT NULL,`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY);

create table `Student` (`firstName` VARCHAR(512) NOT NULL,`lastName` VARCHAR(512) NOT NULL,`middleName` TEXT,`emailAddress` TEXT NOT NULL,`age` INTEGER NOT NULL,`admission` BOOLEAN NOT NULL,`createDate` TEXT NOT NULL,`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY);

alter table `Coffees` add constraint `supplier_fk` foreign key(`supplierId`) references `Suppliers`(`id`) on update NO ACTION on delete CASCADE;