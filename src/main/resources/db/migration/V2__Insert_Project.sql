insert into ProjectTable (name, startDate, duration, projectStatus, id) values ('Bridge Renovation', '2020-05-02T18:31:05.024', 7, 'ON', 1);
insert into ProjectTable (name, startDate, duration, projectStatus, id) values ('Electricity slickDb.Project', '2020-03-14T18:31:05.024', 6, 'ON', 2);
insert into ProjectTable (name, startDate, duration, projectStatus, id) values ('Road Maintenance', '2020-03-14T18:31:05.026', 27, 'PN', 3);
insert into ProjectTable (name, startDate, duration, projectStatus, id) values ('Waterways Maintenance', '2019-12-02T18:31:05.026', 20, 'CM', 4);
insert into ProjectTable (name, startDate, duration, projectStatus, id) values ('Building Maintenance', '2018-05-02T18:31:05.027', 20, 'PS', 5);

insert into ProjectTable (name, startDate, duration, projectStatus, id) values ('Transformer Renovation', '2020-05-02T18:31:05.024', 23, 'ON', 6);
insert into ProjectTable (name, startDate, duration, projectStatus, id) values ('Housing slickDb.Project', '2018-03-14T18:31:05.024', 6, 'ON', 7);
insert into ProjectTable (name, startDate, duration, projectStatus, id) values ('Road Repair', '2019-03-14T18:31:05.026', 27, 'ON', 8);
insert into ProjectTable (name, startDate, duration, projectStatus, id) values ('Boat Maintenance', '2019-12-02T18:31:05.026', 20, 'CM', 9);
insert into ProjectTable (name, startDate, duration, projectStatus, id) values ('RoofShed Maintenance', '2018-05-02T18:31:05.027', 20, 'PS', 10);
