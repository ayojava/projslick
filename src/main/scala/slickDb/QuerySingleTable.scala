package slickDb

import slick.jdbc.MySQLProfile
import slick.lifted.{MappedProjection, TableQuery}
import slick.jdbc.MySQLProfile.api._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Await

object QuerySingleTable extends App{

  private val mysqlDBConfig: MySQLProfile.backend.Database = Database.forConfig("mysqlDB")
  lazy val studentTblQuery: TableQuery[StudentTable] = TableQuery[StudentTable]

  // Helper method for running a query in this example file:
  def exec[T](dbOperation: DBIO[T]): T =  Await.result(mysqlDBConfig.run(dbOperation), 5000.milliseconds)

  try{

    //select all the rows
    val selectAllRowsQryString = studentTblQuery.result.statements.mkString
    println(s"\n Select All rows Query :::: $selectAllRowsQryString \n")
    exec(studentTblQuery.result) foreach { println }

    //Filter by a Single column
    val filterByAdmissionQry: Query[StudentTable, Student, Seq] = studentTblQuery.filter(_.admission === false)
    val filterByAdmissionQryString = filterByAdmissionQry.result.statements.mkString
    println(s"\n Select All rows Query :::: $filterByAdmissionQryString \n")
    exec(filterByAdmissionQry.result) foreach { println }

    //Filter by more than one column
    val filterByMoreThanOneColumnQry = studentTblQuery.filter(aStudent => aStudent.admission === true && aStudent.age > 10)
    val filterByMoreThanOneColumnQryString = filterByMoreThanOneColumnQry.result.statements.mkString
    println(s"\n Filter by More Than One Column :::: $filterByMoreThanOneColumnQryString \n")
    exec(filterByMoreThanOneColumnQry.result) foreach { println }

    //map to single column
    val mapToSingleColumnQry: Query[Rep[String], String, Seq] = studentTblQuery.map(_.emailAddress)
    val mapToSingleColumnQryString = mapToSingleColumnQry.result.statements.mkString
    println(s"\n Map to a single column :::: $mapToSingleColumnQryString \n")
    exec(mapToSingleColumnQry.result) foreach { println }

    //map to multiple columns
    val mapToMultipleColumnsQry: Query[(Rep[Int], Rep[Boolean], Rep[String]), (Int, Boolean, String), Seq]
      = filterByMoreThanOneColumnQry.map(aStudent => (aStudent.age, aStudent.admission, aStudent.firstName ++ " " ++ aStudent.lastName))
    val mapToMultipleColumnsQryString = mapToMultipleColumnsQry.result.statements.mkString
    println(s"\n Map to a single column :::: $mapToMultipleColumnsQryString \n")
    exec(mapToMultipleColumnsQry.result) foreach { println }

    //mapToCaseClass
    case class Sample(id : Long,firstName : String , lastName : String , middleName : Option[String])

    val mapToCaseClassQry: Query[MappedProjection[Sample, (Long, String, String, Option[String])], Sample, Seq]
        = filterByAdmissionQry.map(t => (t.id, t.firstName, t.lastName, t.middleName).mapTo[Sample])
    val mapToCaseClassQryString = mapToCaseClassQry.result.statements.mkString
    println(s"\n Map to a Case class :::: $mapToCaseClassQryString \n")
    exec(mapToCaseClassQry.result) foreach { println }

    //sort by single column
    val sortDescBySingleColumnQry = filterByMoreThanOneColumnQry.map(aStudent => (aStudent.age, aStudent.admission, aStudent.firstName ++ " " ++ aStudent.lastName))
      .sortBy(_._1.desc)
    val sortDescBySingleColumnQryString = sortDescBySingleColumnQry.result.statements.mkString
    println(s"\n Sort by Descending Order :::: $sortDescBySingleColumnQryString \n")
    exec(sortDescBySingleColumnQry.result) foreach { println }

    //sort by multiple columns
    val sortByMultipleColumnsQry= studentTblQuery.sortBy(aStd => (aStd.firstName, aStd.age))
    val sortByMultipleColumnsQryString = sortByMultipleColumnsQry.result.statements.mkString
    println(s"\n Sort by multiple columns :::: $sortByMultipleColumnsQryString \n")
    exec(sortByMultipleColumnsQry.result) foreach { println }

    //Aggregates
    val minAgeQry: Rep[Option[Int]] = studentTblQuery.map(_.age).min
    val minAgeQryString = minAgeQry.shaped.result.statements.head
    println(s"\n Minimum Age Qry String :::: $minAgeQryString \n")
    exec(minAgeQry.result) foreach { println }

    val maxAgeQry: Rep[Option[Int]] = studentTblQuery.map(_.age).max
    val maxAgeQryString = maxAgeQry.shaped.result.statements.head
    println(s"\n Maximum Age Qry String :::: $maxAgeQryString \n")
    exec(maxAgeQry.result) foreach { println }

    val sumOfAgesQry: Rep[Option[Int]] = studentTblQuery.map(_.age).sum
    val sumOfAgesQryString = sumOfAgesQry.shaped.result.statements.head
    println(s"\n Sum of Ages Qry String :::: $sumOfAgesQryString \n")
    exec(sumOfAgesQry.result) foreach { println }

    val averageAgeQry: Rep[Option[Int]] = studentTblQuery.map(_.age).avg
    val averageAgeQryString = averageAgeQry.shaped.result.statements.head
    println(s"\n Sum of Ages Qry String :::: $averageAgeQryString \n")
    exec(averageAgeQry.result) foreach { println }

    //
    /*
    val filterByMoreThanOneColumnQry_ = for {
      query <- studentTblQuery.filter(aStudent => aStudent.admission === false || aStudent.age < 50)
    } yield query
    val filterByMoreThanOneColumnQryString_ = filterByMoreThanOneColumnQry_.result.statements.mkString
    println(s"\n Filter by More Than One Column :::: $filterByMoreThanOneColumnQryString_ \n")
    */
  }finally mysqlDBConfig.close

}
