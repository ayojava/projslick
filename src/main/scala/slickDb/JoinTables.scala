package slickDb


import java.time.LocalDateTime
import slick.jdbc.MySQLProfile.api._

import slickDb.PKs.{EmployeePK, ProjectPK}
import slickDb.ImplicitMapper._


case class Project(name: String , startDate : Option[LocalDateTime] , duration : Option[Long] , projectStatus: ProjectStatus,
                   id : ProjectPK = ProjectPK(0L))

final class ProjectTable(tag: Tag) extends Table[Project](tag,"ProjectTable"){

  def id : Rep[ProjectPK] = column[ProjectPK]("id", O.PrimaryKey, O.AutoInc)

  def name: Rep[String] = column[String]("name")

  def startDate:Rep[Option[LocalDateTime]] = column[Option[LocalDateTime]]("startDate")

  def duration:Rep[Option[Long]] = column[Option[Long]]("duration")

  def projectStatus: Rep[ProjectStatus] = column[ProjectStatus]("projectStatus")

  override def * = ( name , startDate , duration , projectStatus,id).mapTo[Project]
}





case class Employee(firstName: String , lastName : String , gender : String , employeeLevel: EmployeeLevel ,
                    resumptionDate : LocalDateTime , id : EmployeePK =EmployeePK(0L)) {

}
final class EmployeeTable(tag: Tag) extends Table[Employee](tag,"EmployeeTable"){

  def id : Rep[EmployeePK] = column[EmployeePK]("id", O.PrimaryKey, O.AutoInc)

  def firstName: Rep[String] = column[String]("firstName")

  def lastName: Rep[String] = column[String]("lastName")

  def gender: Rep[String] = column[String]("gender")

  def employeeLevel: Rep[EmployeeLevel] = column[EmployeeLevel]("employeeLevel")

  def resumptionDate:Rep[LocalDateTime] = column[LocalDateTime]("resumptionDate")

  override def * = (firstName,lastName,gender,employeeLevel,resumptionDate,id).mapTo[Employee]
}




case class EmployeeProject(employeeId : EmployeePK , projectId : ProjectPK)

final class EmployeeProjectTable(tag: Tag) extends Table[EmployeeProject](tag,"EmployeeProject"){

  def employeeId = column[EmployeePK]("employeeId")

  def projectId = column[ProjectPK]("projectId")

  def pk = primaryKey("employee_project_pk", (employeeId, projectId))

  override def * = (employeeId,projectId).mapTo[EmployeeProject]

  def employeeFK =
    foreignKey("employeeFK" , employeeId, TableQuery[EmployeeTable]) (_.id ,onDelete= ForeignKeyAction.Cascade)

  def projectFK =
    foreignKey("projectFK" , projectId, TableQuery[ProjectTable]) (_.id ,onDelete= ForeignKeyAction.Cascade)
}








