package object slickDb {

  import slick.jdbc.MySQLProfile.api._

  object PKs{
    import slick.lifted.MappedTo

    case class EmployeePK(value: Long) extends AnyVal with MappedTo[Long]
    case class ProjectPK(value: Long) extends AnyVal with MappedTo[Long]
  }

  sealed trait EmployeeLevel
  case object JuniorStaff extends EmployeeLevel
  case object SeniorStaff extends EmployeeLevel
  case object ManagementStaff extends EmployeeLevel

  object EmployeeHelper{
    def typeOfEmployeeLevel(level: String) : EmployeeLevel = level match {
      case "J" => JuniorStaff
      case "M" => ManagementStaff
      case "S" => SeniorStaff
      case _ => throw new IllegalStateException("Illegal Level type: " + level)
    }
  }

  sealed trait ProjectStatus
  case object Pending extends ProjectStatus
  case object Ongoing extends ProjectStatus
  case object Completed extends ProjectStatus
  case object Paused extends ProjectStatus

  object ImplicitMapper{

    implicit val EmployeeLevelMapper = MappedColumnType.base[EmployeeLevel,String](
      employee => employee match {
        case JuniorStaff => "Junior"
        case SeniorStaff => "Senior"
        case ManagementStaff => "Management"
      },
      level => level match {
        case "Junior" => JuniorStaff
        case "Senior" => SeniorStaff
        case "Management" => ManagementStaff
      }
    )

    implicit val ProjectStatusMapper = MappedColumnType.base[ProjectStatus, String](
      status => status match {
        case Pending => "PN"
        case Ongoing => "ON"
        case Completed => "CM"
        case Paused => "PS"
      },
      code => code match {
        case "PN" => Pending
        case "ON" => Ongoing
        case "CM" => Completed
        case "PS" => Paused
      }
    )
  }
}
