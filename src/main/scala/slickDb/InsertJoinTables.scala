package slickDb

import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

import slick.dbio.{DBIO, Effect}
import slick.jdbc.MySQLProfile
import slick.lifted.TableQuery
import slick.jdbc.MySQLProfile.api._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Await

object InsertJoinTables  extends App {

  private val mysqlDBConfig: MySQLProfile.backend.Database = Database.forConfig("mysqlDB")

  lazy val projectTblQuery : TableQuery[ProjectTable] = TableQuery[ProjectTable]

  lazy val employeeTblQuery : TableQuery[EmployeeTable] = TableQuery[EmployeeTable]

  lazy val empProjectTblQuery : TableQuery[EmployeeProjectTable] = TableQuery[EmployeeProjectTable]

  // Helper method for running a query in this example file:
  def exec[T](dbIOperation: DBIO[T]): T =
    Await.result(mysqlDBConfig.run(dbIOperation), 5000.milliseconds)

  def projectSeq: Seq[Project] = Seq(
    Project(name = "Bridge Renovation" ,startDate = Some(LocalDateTime.now()) , Some(7) , projectStatus = Ongoing),
    Project(name = "Electricity Project" ,startDate = Some(LocalDateTime.now().minusWeeks(7)) , Some(6) , projectStatus = Ongoing),
    Project(name = "Road Maintenance" ,startDate = Some(LocalDateTime.now().minusWeeks(7)) , Some(27) , projectStatus = Pending),
    Project(name = "Waterways Maintenance" ,startDate = Some(LocalDateTime.now().minus(5, ChronoUnit.MONTHS)) , Some(20) , projectStatus = Completed),
    Project(name = "Building Maintenance" ,startDate = Some(LocalDateTime.now().minus(2, ChronoUnit.YEARS)) , Some(20) , projectStatus = Paused)
  )

  def employeeSeq = Seq (
    Employee(firstName = "Daniel" , lastName = "Craig" , gender ="Male" ,employeeLevel = JuniorStaff, LocalDateTime.of(2010,11,24,0,0)),

    Employee(firstName = "Faye" , lastName = "Priscilla" , gender ="Female" ,employeeLevel = SeniorStaff,LocalDateTime.of(2012,1,14,0,0)),

    Employee(firstName = "James" , lastName = "Brown" , gender ="Male" ,employeeLevel = SeniorStaff,LocalDateTime.of(2010,1,14,0,0)),

    Employee(firstName = "Felix" , lastName = "Learie" , gender ="Male" ,employeeLevel = ManagementStaff,LocalDateTime.of(2010,1,14,0,0)),

    Employee(firstName = "Sydney" , lastName = "Stone" , gender ="Female" ,employeeLevel = SeniorStaff,LocalDateTime.of(2010,1,14,0,0)),

    Employee(firstName = "Mathew" , lastName = "Ighodala" , gender ="Male" ,employeeLevel = ManagementStaff,LocalDateTime.of(2010,1,14,0,0)),

    Employee(firstName = "Julius" , lastName = "Peter" , gender ="Male" ,employeeLevel = SeniorStaff,LocalDateTime.of(2010,1,14,0,0)),

    Employee(firstName = "Priscillia" , lastName = "Maltilda" , gender ="Female" ,employeeLevel = JuniorStaff,LocalDateTime.of(2010,1,14,0,0)),

    Employee(firstName = "Lovette" , lastName = "Uche" , gender ="Female" ,employeeLevel = ManagementStaff ,LocalDateTime.of(2010,1,14,0,0)),

    Employee(firstName = "Imaculate" , lastName = "Adeyemi" , gender ="Male" ,employeeLevel = JuniorStaff,LocalDateTime.of(2010,1,14,0,0)),

    Employee(firstName = "Harrison" , lastName = "Olugbenga" , gender ="Male" ,employeeLevel = SeniorStaff,LocalDateTime.of(2010,1,14,0,0)),
  )


  val insertProjectAction = projectTblQuery returning projectTblQuery.map(_.id) into {
    (project , projectId) => project.copy(id = projectId)
  }

  val insertEmployeeAction = employeeTblQuery returning employeeTblQuery.map(_.id) into {
    (employee , employeeId) => employee.copy(id = employeeId)
  }

  private val employeeProjectAction: DBIOAction[Option[Int], NoStream, Effect.Schema with Effect.Write] = for {
    //_: Unit                       <- DBIO.seq( ddlSchemaQuery.create   ) Not necessary
    allProjects: Seq[Project]     <- insertProjectAction ++= projectSeq
    allEmployees: Seq[Employee]   <- insertEmployeeAction ++= employeeSeq
    rowCount: Option[Int] <- empProjectTblQuery ++= populateJoinTable(allProjects,allEmployees)
  } yield rowCount

  /*
  private val employeeProjectAction_Map: DBIOAction[Option[Int], NoStream, Effect.Schema with Effect.Write] =
    DBIO.seq(ddlSchemaQuery.dropIfExists, ddlSchemaQuery.createIfNotExists).flatMap((_: Unit) =>
        (insertProjectAction ++= projectSeq).flatMap((allProjects: Seq[Project]) =>
            (insertEmployeeAction ++= employeeSeq).flatMap((allEmployees: Seq[Employee]) =>
                (empProjectTblQuery ++= populateJoinTable(allProjects, allEmployees)).map((rowCount: Option[Int]) => rowCount)
              )
          )
      )
  */

  def populateJoinTable (projectSeq : Seq[Project] , employeeSeq : Seq[Employee]) =
    for {
      aProject <- projectSeq
      aEmployee <- employeeSeq
    }yield EmployeeProject(aEmployee.id,aProject.id)

  def populateJoinTable2 (projectSeq : Seq[Project] , employeeSeq : Seq[Employee]) =
    projectSeq
      .flatMap(aProject =>
        employeeSeq
          .map(aEmployee => EmployeeProject(aEmployee.id, aProject.id))
      )



  try{

    println("\n \n Inserting Data for Join Relationship")
    val rowsCountOption: Option[Int] = exec(employeeProjectAction)
    rowsCountOption match {
      case Some(insertRowsCount) => println(s"No of Inserted Rows ::: $insertRowsCount")
      case None => println("No row was inserted")
    }
  }finally mysqlDBConfig.close
}
