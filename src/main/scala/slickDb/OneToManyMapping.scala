package slickDb

import java.time.LocalDateTime

import slick.dbio.Effect
import slick.jdbc.MySQLProfile
import slick.lifted.TableQuery
import slick.jdbc.MySQLProfile.api._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Await

object OneToManyMapping extends App {

  lazy val suppliersTblQuery: TableQuery[SuppliersTable] = TableQuery[SuppliersTable]

  lazy val coffeesTblQuery: TableQuery[CoffeesTable] = TableQuery[CoffeesTable]


  lazy val suppliersFKQry: Query[SuppliersTable, Suppliers, Seq] = coffeesTblQuery.flatMap(_.supplierFK)

  println(s"\n\n Query String :: ${suppliersFKQry.result.statements.mkString}")

  private val mysqlDBConfig: MySQLProfile.backend.Database = Database.forConfig("mysqlDB")

  // Helper method for running a query in this example file:
  def exec[T](dbOperation: DBIO[T]): T =
    Await.result(mysqlDBConfig.run(dbOperation), 5000.milliseconds)

  val insertAndReturnSuplierAction = suppliersTblQuery returning suppliersTblQuery.map(_.id) into {
    (supplier , supplierId) => supplier.copy(id = supplierId)
  }

  val insertAndReturnCoffeeAction = coffeesTblQuery returning coffeesTblQuery.map(_.id) into {
    (coffee, coffeeId ) => coffee.copy(id = coffeeId)
  }


  val supplier1 =
    Suppliers( name = " Merchandise Systems " , street = "Meiran" ,city = "Ojokoro", state = "Lagos State" , zip = Some("23401"))

  def coffeesSeqData(aSupplierId : Long): Seq[Coffees] = {
    val coffeeSeq = Seq(
      Coffees(name = "Expresso", price = 50.34, qty = 56, createDate = LocalDateTime.now(), supplierId = aSupplierId),
      Coffees(name = "Nescafe", price = 44.34, qty = 29, createDate = LocalDateTime.now(), supplierId = aSupplierId),
      Coffees(name = "Lipton", price = 234.23, qty = 44, createDate = LocalDateTime.now(), supplierId = aSupplierId),
      Coffees(name = "Jam", price = 43.09, qty = 20, createDate = LocalDateTime.now(), supplierId = aSupplierId),
      Coffees(name = "Ice Tea", price = 22.34, qty = 23, createDate = LocalDateTime.now(), supplierId = aSupplierId)
    )

    coffeeSeq
  }


  try{

    println(s"\n Query Table :::: ${suppliersFKQry.result.statements.mkString} \n")

    /*
    No Longer Required
    println("\n \n Inserting Data for OneToMany Relationship")

    val tupleResultAction: DBIOAction[(Suppliers, Seq[Coffees]), NoStream, Effect.Write] = for {
      supplierData <- insertAndReturnSuplierAction += supplier1
      coffeeSeqDataResult <- insertAndReturnCoffeeAction ++= coffeesSeqData(supplierData.id)
    } yield (supplierData, coffeeSeqDataResult)

    val (aSupplier, coffeeSeq) = exec(tupleResultAction)
    println(s" Supplier ::: $aSupplier \n")
    coffeeSeq.foreach(aCoffee => println(s"Coffee :::: $aCoffee"))

    println("\nState of the Supplier Table :")
    exec(suppliersTblQuery.result.map(_.foreach(println)))

    println("\nState of the Coffee Table :")
    exec(coffeesTblQuery.result.map(_.foreach(println)))
*/


  }finally mysqlDBConfig.close
}
