package slickDb
import java.time.LocalDateTime

import slick.jdbc.MySQLProfile
import slick.lifted.{MappedProjection, TableQuery}
import slick.jdbc.MySQLProfile.api._

import scala.concurrent.duration._
import scala.concurrent.Await



object QueryJoinTables extends App {

  private val mysqlDBConfig: MySQLProfile.backend.Database = Database.forConfig("mysqlDB")

  lazy val projectTblQuery : TableQuery[ProjectTable] = TableQuery[ProjectTable]

  lazy val employeeTblQuery : TableQuery[EmployeeTable] = TableQuery[EmployeeTable]

  lazy val empProjectTblQuery : TableQuery[EmployeeProjectTable] = TableQuery[EmployeeProjectTable]

  //Monadic Join (Navigating using Foreign Keys ) - Inner Join
  private val allTablesQry: Query[(EmployeeTable, EmployeeProjectTable, ProjectTable), (Employee, EmployeeProject, Project), Seq] =
    for {
      employee <- employeeTblQuery
      employeeProject <- empProjectTblQuery if employee.id === employeeProject.employeeId
      project <- projectTblQuery if employeeProject.projectId === project.id
  } yield (employee, employeeProject, project)

  //Monadic Join (Navigating using Foreign Keys ) - Inner Join
  private val allTablesQry2: Query[(EmployeeTable, EmployeeProjectTable, ProjectTable), (Employee, EmployeeProject, Project), Seq] = for {
    employeeProject <- empProjectTblQuery
    employee <- employeeProject.employeeFK
    project <- employeeProject.projectFK
  } yield (employee, employeeProject, project)


  //Monadic Join (Navigating without Foreign Keys ) - Cross Join
  private val allTablesQry3: Query[(EmployeeTable, EmployeeProjectTable, ProjectTable), (Employee, EmployeeProject, Project), Seq] = for {
    employeeProject <- empProjectTblQuery
    project <- projectTblQuery
    employee <- employeeTblQuery
  } yield (employee, employeeProject, project)


  //Selecting which columns to return
  private val allTablesQry4 : Query[(Rep[String], Rep[String], Rep[PKs.EmployeePK], Rep[PKs.ProjectPK], Rep[Option[LocalDateTime]], Rep[String]), (String, String, PKs.EmployeePK, PKs.ProjectPK, Option[LocalDateTime], String), Seq] = allTablesQry.map {
    case (employee, employeeProject, project) =>
      (employee.firstName, employee.lastName, employeeProject.employeeId, employeeProject.projectId, project.startDate,
        project.name)

  }

  //InnerJoin
  private val allTablesQryInnerJoin: Query[((EmployeeTable, EmployeeProjectTable), ProjectTable), ((Employee, EmployeeProject), Project), Seq] =
      employeeTblQuery
        .join(empProjectTblQuery).on(_.id === _.employeeId)
        .join(projectTblQuery).on(_._2.projectId === _.id)


  val allTablesQryJoinsMap: Query[(Rep[PKs.EmployeePK], Rep[String], Rep[String]), (PKs.EmployeePK, String, String), Seq] = allTablesQryInnerJoin.map {
    case (tuple: (EmployeeTable, EmployeeProjectTable), _) => (tuple._1.id, tuple._1.firstName , tuple._1.lastName)
  }


  case class Sample(firstName:String , lastName: String , gender : String ,projectName : String)

  private val sampleCaseClassMapping: Query[MappedProjection[Sample, (String, String, String, String)], Sample, Seq] = allTablesQryInnerJoin.map {
    case (tuple: (EmployeeTable, EmployeeProjectTable), table) =>
      (tuple._1.firstName, tuple._1.lastName, tuple._1.gender, table.name).mapTo[Sample]
  }


  //Add a filter
  private val allTablesQry4Filter : Query[(Rep[String], Rep[String], Rep[PKs.EmployeePK], Rep[PKs.ProjectPK], Rep[Option[LocalDateTime]], Rep[String]), (String, String, PKs.EmployeePK, PKs.ProjectPK, Option[LocalDateTime], String), Seq] =
    allTablesQry.map {
    case (employee, employeeProject, project) =>
      (employee.firstName, employee.lastName, employeeProject.employeeId, employeeProject.projectId, project.startDate,
        project.name)
  }.filter{
      case (_, _, _, _, _, projectName) => projectName === "Road Repair"
    }


  private val filterQryOnInnerJoin: Query[((EmployeeTable, EmployeeProjectTable), ProjectTable), ((Employee, EmployeeProject), Project), Seq] =
    allTablesQryInnerJoin.filter {
    case (_, projectTable) => projectTable.duration > 20L
  }

  //JoinLeft ****
  private val employee_JoinLeft_EmployeeProjectQry: Query[(EmployeeTable, Rep[Option[EmployeeProjectTable]]), (Employee, Option[EmployeeProject]), Seq] =
    employeeTblQuery.joinLeft(empProjectTblQuery).on(_.id === _.employeeId)

  //JoinFull
  private val employeeProject_FullJoin_Project: Query[(Rep[Option[String]], Rep[Option[Option[Long]]], Rep[Option[PKs.EmployeePK]]), (Option[String], Option[Option[Long]], Option[PKs.EmployeePK]), Seq] =
    empProjectTblQuery.joinFull(projectTblQuery).on(_.projectId === _.id).map{
      case (empProject, project) => (project.map(_.name) , project.map(_.duration) , empProject.map(_.employeeId))
    }

  //CrossJoin
  private val employee_CrossJoin_EmployeeProjectQry: Query[(EmployeeTable, Rep[Option[EmployeeProjectTable]]), (Employee, Option[EmployeeProject]), Seq] =
    employeeTblQuery.joinLeft(empProjectTblQuery)


  // Helper method for running a query in this example file:
  def exec[T](dbOperation: DBIO[T]): T =  Await.result(mysqlDBConfig.run(dbOperation), 5000.milliseconds)

  try{
    val filterQryOnInnerJoinString = filterQryOnInnerJoin.result.statements.mkString
    println(s"\n 1 -Query Table :::: $filterQryOnInnerJoinString \n")

    // Helper method for running a query in this example file:
    def exec[T](dbOperation: DBIO[T]): T =
      Await.result(mysqlDBConfig.run(dbOperation), 5000.milliseconds)

    exec(filterQryOnInnerJoin.result) foreach { println }
    val rowCount: Int = exec(filterQryOnInnerJoin.length.result)
    println(s"$rowCount")

  }finally mysqlDBConfig.close

}
