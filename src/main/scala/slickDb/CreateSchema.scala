package slickDb

import slick.jdbc.MySQLProfile
import slick.jdbc.MySQLProfile.api._
import slick.lifted.TableQuery

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._

object CreateSchema extends App{

  private val mysqlDBConfig: MySQLProfile.backend.Database = Database.forConfig("mysqlDB")


  lazy val suppliersTblQuery: TableQuery[SuppliersTable] = TableQuery[SuppliersTable]

  lazy val coffeesTblQuery: TableQuery[CoffeesTable] = TableQuery[CoffeesTable]

  lazy val studentTblQuery: TableQuery[StudentTable] = TableQuery[StudentTable]



  private val ddlSchemaQuery = suppliersTblQuery.schema ++ coffeesTblQuery.schema ++ studentTblQuery.schema

  println(" \n \n Create  Statements ")
  ddlSchemaQuery.create.statements.foreach(println)

  println(" \n \n CreateIfNotExists  Statements ")
  ddlSchemaQuery.createIfNotExists.statements.foreach(println)

  println(" \n \n Truncate  Statements ")
  ddlSchemaQuery.truncate.statements.foreach(println)

  println(" \n \n Drop  Statements ")
  ddlSchemaQuery.drop.statements.foreach(println)

  println(" \n \n Drop If Exists  Statements ")
  ddlSchemaQuery.dropIfExists.statements.foreach(println)

  println("\n")


  private val schemaActions =  DBIO.seq( ddlSchemaQuery.dropIfExists , ddlSchemaQuery.createIfNotExists )


  private val futureResponse: Future[Unit] = mysqlDBConfig.run(schemaActions)
  private val unitResponse:  Unit = Await.result(futureResponse, 2.seconds)

}
