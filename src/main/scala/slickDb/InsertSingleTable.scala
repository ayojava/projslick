package slickDb

import java.time.LocalDateTime

import slick.jdbc.MySQLProfile
import slick.lifted.TableQuery
import slick.jdbc.MySQLProfile.api._
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Await

/*
This class demonstrates CRUD operations on Single Table
*/
object InsertSingleTable  extends App {

  lazy val studentTblQuery: TableQuery[StudentTable] = TableQuery[StudentTable]

  private val mysqlDBConfig: MySQLProfile.backend.Database = Database.forConfig("mysqlDB")


  // Helper method for running a query in this example file:
  def exec[T](dbIOperation: DBIO[T]): T =
    Await.result(mysqlDBConfig.run(dbIOperation), 5000.milliseconds)


  // Utility to print out what is in the database:
  def printStudentDatabaseState() = {
    println("\nState of the Student database:")
    exec(studentTblQuery.result.map(_.foreach(println)))
  }

  try{

    println("\n\n Inserting Single data for student")
    val student1 = Student("Ayodeji","Ilori",Some("Oluwaseun") ,"ayojava@gmail.com",24, true ,LocalDateTime.now())
    val insertSingleDataAction = studentTblQuery += student1
    val insertRowsCount: Int = exec(insertSingleDataAction)
    println(s"No of Inserted Rows ::: $insertRowsCount")

    println("\n\n Inserting Single data for student and returning Id ")
    val student2 =
      Student("Ayodeji","samuel",Some("Igbunedion") ,"ayo@gmail.com", 34, true ,LocalDateTime.of(2020,12,11,8,8))
    val insertSingleDataReturningIdAction = studentTblQuery returning studentTblQuery.map(_.id)
    val insertSingleDataReturnId =  insertSingleDataReturningIdAction += student2
    val databaseId: Long = exec(insertSingleDataReturnId)
    println(s"Database Id ::: $databaseId")

    println("\n\n Inserting Single data for student and returning row ")
    val student3 = Student("Demola","Ayorinde",Some("Thomas") ,"retyuu@gmail.com", 29, false ,LocalDateTime.of(2019,11,11,8,8))
    val insertSingleDataReturningAction  = studentTblQuery returning studentTblQuery.map(_.id) into {
      (student,id) => student.copy(id = id)
    }

    val insertSingleDataReturnRow = insertSingleDataReturningAction += student3
    val student: Student = exec(insertSingleDataReturnRow)
    println(s"Student Data :::: $student")


    println("\n\n Inserting Multiple data for student")
    def studentSeq = Seq(
      Student("Nath","Xtina",Some("Tyrese") ,"go@gmail.com", 20, true ,LocalDateTime.of(2019,12,12,8,8)),
      Student("Femi","Seoung",Some("Gibson") ,"fedrt@gmail.com", 29, false ,LocalDateTime.of(2019,8,21,8,8)),
      Student("Bayo","Adeyemi",Some("tekken") ,"all@gmail.com", 29, false ,LocalDateTime.of(2022,5,10,8,8))
    )
    val insertMultipleStudentsAction = studentTblQuery ++= studentSeq
    val maybeInt: Option[Int] = exec(insertMultipleStudentsAction)
    println(s"Rows Inserted :::: $maybeInt")

    println("\n\n Inserting Multiple data for student and Returning rows")
    def studentSeq2 = Seq(
      Student("aaaa","eeee",Some("gggg") ,"go@gmail.com", 20, false ,LocalDateTime.of(2000,10,11,8,8)),
      Student("bbbb","dddd",Some("hhhh") ,"fedrt@gmail.com", 29, true ,LocalDateTime.of(2001,2,9,8,22)),
      Student("cccc","ffff",Some("rrrr") ,"all@gmail.com", 29, true ,LocalDateTime.of(2002,12,5,8,8))
    )
    val insertMultipleStudentsReturnRowsAction  = insertSingleDataReturningAction ++= studentSeq2
    val studentSeqData: Seq[Student] = exec(insertMultipleStudentsReturnRowsAction)
    studentSeqData.foreach(aStudent => println(s"Student :::: $aStudent"))

    printStudentDatabaseState()

  }finally mysqlDBConfig.close

}
