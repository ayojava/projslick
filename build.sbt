name := "projslick"

version := "0.1"

scalaVersion := "2.12.8"

enablePlugins(FlywayPlugin)
val dbUser = System.getProperty("db.user", "root")
val dbPass = System.getProperty("db.pass", "ayojava")
val dbUrl = System.getProperty("db.url", "jdbc:mysql://localhost:3306/slickdb")

flywayUrl := dbUrl
flywayUser := dbUser
flywayPassword := dbPass
flywayLocations += "db/migration"



libraryDependencies ++= Seq(
  "com.typesafe.slick" %% "slick" % "3.3.2",

  "org.slf4j" % "slf4j-nop" % "1.7.29",

  "mysql" % "mysql-connector-java" % "8.0.18",

  "com.typesafe.slick" %% "slick-hikaricp" % "3.3.2",

  "org.scalatest" %% "scalatest" % "3.0.8" % "test",

)
